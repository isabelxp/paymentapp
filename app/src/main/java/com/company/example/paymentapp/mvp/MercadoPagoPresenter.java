package com.company.example.paymentapp.mvp;

import android.util.Log;

import com.company.example.paymentapp.interfaces.interfaces;
import com.company.example.paymentapp.models.modelsCardIssuers.CardIssuersResponse;
import com.company.example.paymentapp.models.modelsInstallments.InstallmentsResponse;
import com.company.example.paymentapp.models.modelsPaymentMethos.PaymentMethosResponse;
import com.company.example.paymentapp.retrofit.RetrofitClient;
import com.company.example.paymentapp.utils.Consts;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MercadoPagoPresenter implements MercadoPagoContract.Presenter{

    public MercadoPagoContract.View view;
    private String TAG;
    interfaces serv;

    public MercadoPagoPresenter(MercadoPagoContract.View view){
        this.view=view;
        TAG=this.getClass().getSimpleName();
        serv = RetrofitClient.getClient(Consts.BASE_URL).create(interfaces.class);
    }

    @Override
    public void getPaymentMethods() {

        serv.getPaymentMethods(Consts.PUBLIC_KEY).enqueue(new Callback<List<PaymentMethosResponse>>() {
            @Override
            public void onResponse(Call<List<PaymentMethosResponse>> call, Response<List<PaymentMethosResponse>> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getPaymentMethodsRespoonse(response.body());
                        view.setData(true);
                    }
                }else{
                    view.setData(false);

                }
            }
            @Override
            public void onFailure(Call<List<PaymentMethosResponse>> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+call.request().url());
                Log.i(TAG,"--->failed :( "+t.getCause().getMessage());
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    @Override
    public void getCardIssuers(String payment_id) {

        serv.getCardIssuers(Consts.PUBLIC_KEY,payment_id).enqueue(new Callback<List<CardIssuersResponse>>() {
            @Override
            public void onResponse(Call<List<CardIssuersResponse>> call, Response<List<CardIssuersResponse>> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        view.getCardIssuersResponse(response.body());
                        view.setData(true);
                    }
                }else{

                        view.setData(false);
                }
            }
            @Override
            public void onFailure(Call<List<CardIssuersResponse>> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+call.request().url());
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }

    @Override
    public void getInstallments(String payment_id,String cardissuers,double amount) {

        serv.getInstallments(Consts.PUBLIC_KEY,amount,payment_id,cardissuers).enqueue(new Callback<List<InstallmentsResponse>>() {
            @Override
            public void onResponse(Call<List<InstallmentsResponse>> call, Response<List<InstallmentsResponse>> response) {
                if(response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.d("getInstallments",""+call.request().url());
                        view.getInstallmentsResponse(response.body());
                        view.setData(true);
                    }
                }else{
                    Log.d("getInstallments","failed fallo "+call.request().url());
                        view.setData(false);
                }
            }
            @Override
            public void onFailure(Call<List<InstallmentsResponse>> call, Throwable t) {
                Log.i(TAG,"--->failed :( "+call.request().url());
                Log.i(TAG,"--->failed :( "+t.getMessage());
            }
        });
    }
}
