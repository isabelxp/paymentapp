package com.company.example.paymentapp.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.company.example.paymentapp.R;
import com.company.example.paymentapp.interfaces.interOnclickAdapter;
import com.company.example.paymentapp.models.modelAnswerPayment;
import com.company.example.paymentapp.models.modelsCardIssuers.CardIssuersResponse;
import com.company.example.paymentapp.models.modelsPaymentMethos.PaymentMethosResponse;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentMethodsAdapter extends RecyclerView.Adapter<PaymentMethodsAdapter.ViewHolder>{

    List<PaymentMethosResponse> list;
    interOnclickAdapter inter;
    public PaymentMethodsAdapter(List<PaymentMethosResponse> list, Context context) {
        this.list=list;
        inter=(interOnclickAdapter)context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cards, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Uri uri = Uri.parse(list.get(position).getSecureThumbnail());
        holder.bindData(list.get(position));
        holder.img.setImageURI(uri);
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelAnswerPayment.setPayment_method_id(list.get(position).getId());
                inter.OnclickAdapter();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.textView2)
        TextView textView2;
        @BindView(R.id.imageView)
        SimpleDraweeView img;
        @BindView(R.id.layout)
        ConstraintLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }


        public void bindData(PaymentMethosResponse item) {
        textView2.setText(item.getName());

        }


    }
}
