package com.company.example.paymentapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.company.example.paymentapp.R;
import com.company.example.paymentapp.adapters.CardIssuersAdapter;
import com.company.example.paymentapp.adapters.InstallmentsAdapter;
import com.company.example.paymentapp.models.modelAnswerPayment;
import com.company.example.paymentapp.models.modelsCardIssuers.CardIssuersResponse;
import com.company.example.paymentapp.models.modelsInstallments.InstallmentsResponse;
import com.company.example.paymentapp.models.modelsPaymentMethos.PaymentMethosResponse;
import com.company.example.paymentapp.mvp.MercadoPagoContract;
import com.company.example.paymentapp.mvp.MercadoPagoPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class InstallmentsFragment extends Fragment implements MercadoPagoContract.View{


    public InstallmentsFragment() {
        // Required empty public constructor
    }
    public static InstallmentsFragment newInstance() {
        return new InstallmentsFragment();
    }

    MercadoPagoPresenter presenter;
    InstallmentsAdapter adapter;
    @BindView(R.id.my_recycler)
    RecyclerView recycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_installments, container, false);
        ButterKnife.bind(this, v);
        presenter =new MercadoPagoPresenter(this);
        presenter.getInstallments(modelAnswerPayment.getPayment_method_id(),modelAnswerPayment.getIssuerid(),modelAnswerPayment.getAmount());


        return v;
    }

    @Override
    public void setData(Boolean code) {

    }

    @Override
    public void getCardIssuersResponse(List<CardIssuersResponse> list) {

    }

    @Override
    public void getPaymentMethodsRespoonse(List<PaymentMethosResponse> list) {

    }

    @Override
    public void getInstallmentsResponse(List<InstallmentsResponse> list) {
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new InstallmentsAdapter(list.get(0).getPayerCosts(),getContext());
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(adapter);
    }
}
