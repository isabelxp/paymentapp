package com.company.example.paymentapp.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.example.paymentapp.R;
import com.company.example.paymentapp.interfaces.interOnclickAdapter;
import com.company.example.paymentapp.models.modelAnswerPayment;
import com.company.example.paymentapp.models.modelsCardIssuers.CardIssuersResponse;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CardIssuersAdapter extends RecyclerView.Adapter<CardIssuersAdapter.ViewHolder>{

    List<CardIssuersResponse> list=new ArrayList<>();
    interOnclickAdapter inter;
    public CardIssuersAdapter(List<CardIssuersResponse> list, Context context) {
        this.list=list;
        inter=(interOnclickAdapter)context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cards, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        Uri uri = Uri.parse(list.get(position).getSecureThumbnail());
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(uri)
                .setAutoPlayAnimations(true)
         .build();
        holder.img.setController(controller);


        holder.bindData(list.get(position));
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelAnswerPayment.setIssuer_id(list.get(position).getName());
                modelAnswerPayment.setIssuerid(list.get(position).getId());
                inter.OnclickAdapter();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.textView2)
        TextView textView2;
        @BindView(R.id.imageView)
        SimpleDraweeView img;
        @BindView(R.id.layout)
        ConstraintLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }


        public void bindData(CardIssuersResponse item) {
                    textView2.setText(item.getName());

        }


    }



}
