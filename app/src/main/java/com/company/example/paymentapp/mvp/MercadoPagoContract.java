package com.company.example.paymentapp.mvp;

import com.company.example.paymentapp.models.modelsCardIssuers.CardIssuersResponse;
import com.company.example.paymentapp.models.modelsInstallments.InstallmentsResponse;
import com.company.example.paymentapp.models.modelsPaymentMethos.PaymentMethosResponse;

import java.util.List;

public class MercadoPagoContract {

    public interface View{
        void setData(Boolean code);
        void getCardIssuersResponse(List<CardIssuersResponse> list);
        void getPaymentMethodsRespoonse(List<PaymentMethosResponse> list);
        void getInstallmentsResponse(List<InstallmentsResponse> list);
    }

    public interface Presenter{
        void getPaymentMethods();
        void getCardIssuers(String payment_id);
        void getInstallments(String payment_id,String cardissuers,double amount);
    }

}
