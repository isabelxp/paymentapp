package com.company.example.paymentapp.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.company.example.paymentapp.R;
import com.company.example.paymentapp.adapters.CardIssuersAdapter;
import com.company.example.paymentapp.interfaces.interOnclickAdapter;
import com.company.example.paymentapp.models.modelAnswerPayment;
import com.company.example.paymentapp.models.modelsCardIssuers.CardIssuersResponse;
import com.company.example.paymentapp.models.modelsInstallments.InstallmentsResponse;
import com.company.example.paymentapp.models.modelsPaymentMethos.PaymentMethosResponse;
import com.company.example.paymentapp.mvp.MercadoPagoContract;
import com.company.example.paymentapp.mvp.MercadoPagoPresenter;
import com.facebook.drawee.backends.pipeline.Fresco;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CardIssuersFragment extends Fragment implements MercadoPagoContract.View {


    public CardIssuersFragment() {
        // Required empty public constructor
    }

    public static CardIssuersFragment newInstance() {
        return new CardIssuersFragment();
    }

    MercadoPagoPresenter presenter;
    @BindView(R.id.my_recycler_view)
    RecyclerView recycler;
    CardIssuersAdapter adapter;
    ProgressDialog progress;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_card_issuers, container, false);
        ButterKnife.bind(this, v);
        progress = new ProgressDialog(getActivity());
        progress.setTitle(getContext().getString(R.string.string_loading));
        presenter=new MercadoPagoPresenter(this);

        if (savedInstanceState==null)
        {
            presenter.getCardIssuers(modelAnswerPayment.getPayment_method_id());
            progress.show();
        }

        return v;
    }

    @Override
    public void setData(Boolean code) {
    progress.cancel();
    }

    @Override
    public void getCardIssuersResponse(List<CardIssuersResponse> list) {
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new CardIssuersAdapter(list,getContext());
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(adapter);
    }

    @Override
    public void getPaymentMethodsRespoonse(List<PaymentMethosResponse> list) { }

    @Override
    public void getInstallmentsResponse(List<InstallmentsResponse> list) { }


}
