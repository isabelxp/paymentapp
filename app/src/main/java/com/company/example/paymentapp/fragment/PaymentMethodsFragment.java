package com.company.example.paymentapp.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.company.example.paymentapp.R;
import com.company.example.paymentapp.adapters.CardIssuersAdapter;
import com.company.example.paymentapp.adapters.PaymentMethodsAdapter;
import com.company.example.paymentapp.models.modelsCardIssuers.CardIssuersResponse;
import com.company.example.paymentapp.models.modelsInstallments.InstallmentsResponse;
import com.company.example.paymentapp.models.modelsPaymentMethos.PaymentMethosResponse;
import com.company.example.paymentapp.mvp.MercadoPagoContract;
import com.company.example.paymentapp.mvp.MercadoPagoPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentMethodsFragment extends Fragment implements MercadoPagoContract.View{


    public PaymentMethodsFragment() {
        // Required empty public constructor
    }

    public static PaymentMethodsFragment newInstance() {
        return new PaymentMethodsFragment();
    }

    MercadoPagoPresenter presenter;
    @BindView(R.id.my_recycler)
    RecyclerView recycler;
    PaymentMethodsAdapter adapter;
    ProgressDialog progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_payment_methods, container, false);
        ButterKnife.bind(this, v);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        progress = new ProgressDialog(getContext());
        progress.setTitle(getContext().getString(R.string.string_loading));
        progress.show();

        presenter=new MercadoPagoPresenter(this);
        presenter.getPaymentMethods();
        return v;
    }

    @Override
    public void setData(Boolean code) {
     progress.cancel();
    }

    @Override
    public void getCardIssuersResponse(List<CardIssuersResponse> list) {

    }

    @Override
    public void getPaymentMethodsRespoonse(List<PaymentMethosResponse> list) {

        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new PaymentMethodsAdapter(list,getContext());
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(adapter);
    }

    @Override
    public void getInstallmentsResponse(List<InstallmentsResponse> list) { }
}
