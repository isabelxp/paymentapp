package com.company.example.paymentapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.company.example.paymentapp.R;
import com.company.example.paymentapp.models.modelAnswerPayment;
import com.company.example.paymentapp.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class AmountFragment extends Fragment {


    public AmountFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.textView)
    TextView textView;

    @BindView(R.id.editText)
    EditText editText;


    public static AmountFragment newInstance() {

        return new AmountFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_amount, container, false);
        ButterKnife.bind(this, view);

        if(modelAnswerPayment.getStatusDialog())
        {
            Utils.getInstance(getContext()).createDialog(modelAnswerPayment.getPayment_method_id(),
                    modelAnswerPayment.getIssuer_id(),
                    modelAnswerPayment.getInstallments());
            modelAnswerPayment.setStatusDialog(false);
        }


        editChange();


        return view;
    }

    public void editChange()
    {
        editText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) { }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                modelAnswerPayment.setAmount(Integer.parseInt(editText.getText().toString()));
            }
        });
    }



}
