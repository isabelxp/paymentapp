package com.company.example.paymentapp.models;

public class modelAnswerPayment {

    public static String payment_method_id;
    public static double amount;
    public static String issuer_id;
    public static String issuerid;
    public static String installments;
    public static Boolean statusDialog=false;


    public static String getIssuerid() {
        return issuerid;
    }

    public static void setIssuerid(String issuerid) {
        modelAnswerPayment.issuerid = issuerid;
    }

    public static String getInstallments() {
        return installments;
    }

    public static void setInstallments(String installments) {
        modelAnswerPayment.installments = installments;
    }

    public static Boolean getStatusDialog() {
        return statusDialog;
    }

    public static void setStatusDialog(Boolean statusDialog) {
        modelAnswerPayment.statusDialog = statusDialog;
    }

    public static String getPayment_method_id() {
        return payment_method_id;
    }

    public static void setPayment_method_id(String payment_method_id) {
        modelAnswerPayment.payment_method_id = payment_method_id;
    }

    public static double getAmount() {
        return amount;
    }

    public static void setAmount(double amount) {
        modelAnswerPayment.amount = amount;
    }

    public static String getIssuer_id() {
        return issuer_id;
    }

    public static void setIssuer_id(String issuer_id) {
        modelAnswerPayment.issuer_id = issuer_id;
    }
}
