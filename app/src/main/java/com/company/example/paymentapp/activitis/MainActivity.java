package com.company.example.paymentapp.activitis;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;
import android.widget.Toolbar;

import com.company.example.paymentapp.R;
import com.company.example.paymentapp.fragment.AmountFragment;
import com.company.example.paymentapp.fragment.CardIssuersFragment;
import com.company.example.paymentapp.fragment.InstallmentsFragment;
import com.company.example.paymentapp.fragment.PaymentMethodsFragment;
import com.company.example.paymentapp.interfaces.interOnclickAdapter;
import com.company.example.paymentapp.models.modelAnswerPayment;
import com.company.example.paymentapp.models.modelsCardIssuers.CardIssuersResponse;
import com.company.example.paymentapp.models.modelsPaymentMethos.PaymentMethosResponse;
import com.company.example.paymentapp.mvp.MercadoPagoContract;
import com.company.example.paymentapp.mvp.MercadoPagoPresenter;
import com.company.example.paymentapp.utils.Utils;
import com.facebook.drawee.backends.pipeline.Fresco;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements interOnclickAdapter {

    @BindView(R.id.button)
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Fresco.initialize(this);
        modelAnswerPayment.setAmount(0);

            Utils.addFragmentToActivity(getSupportFragmentManager(),
                    new AmountFragment(), R.id.contentFrame);


    }

    @OnClick(R.id.button)
    public void singIn() {
        if(modelAnswerPayment.getAmount()>=100)
        {
            fragmentChange();
            button.setVisibility(View.GONE);

        }else
        {
            alerta();
        }
    }

    @Override
    public void OnclickAdapter() {
        fragmentChange();
    }

    public void fragmentChange()
    {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        Fragment fragment=new PaymentMethodsFragment();
        Fragment fragment1=new CardIssuersFragment();
        Fragment fragment2=new InstallmentsFragment();

        if (!currentFragment.getClass().getName().equalsIgnoreCase(fragment.getClass().getName())) {
            Utils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.contentFrame);
        }
        if (currentFragment.getClass().getName().equalsIgnoreCase(fragment.getClass().getName())) {
            Utils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment1, R.id.contentFrame);
        }
        if (currentFragment.getClass().getName().equalsIgnoreCase(fragment1.getClass().getName())) {
            Utils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment2, R.id.contentFrame);
        }
        if (currentFragment.getClass().getName().equalsIgnoreCase(fragment2.getClass().getName())) {
            Utils.addFragmentToActivity(getSupportFragmentManager(),
                    new AmountFragment(), R.id.contentFrame);
            button.setVisibility(View.VISIBLE);
        }
    }

    public void alerta()
    {
        final AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(getString(R.string.string_message))
                .setMessage(getString(R.string.string_message_detail))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onBackPressed() {

    }
}
