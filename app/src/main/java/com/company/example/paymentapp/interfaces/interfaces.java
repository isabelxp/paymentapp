package com.company.example.paymentapp.interfaces;

import com.company.example.paymentapp.models.modelsCardIssuers.CardIssuersResponse;
import com.company.example.paymentapp.models.modelsInstallments.InstallmentsResponse;
import com.company.example.paymentapp.models.modelsPaymentMethos.PaymentMethosResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface interfaces {

    @GET("payment_methods")
    Call<List<PaymentMethosResponse>> getPaymentMethods(@Query("public_key") String public_key);

    @GET("payment_methods/card_issuers")
    Call<List<CardIssuersResponse>> getCardIssuers(@Query("public_key") String public_key,@Query("payment_method_id") String payment_method_id);

    @GET("payment_methods/installments")
    Call<List<InstallmentsResponse>> getInstallments(@Query("public_key") String public_key, @Query("amount") double amount, @Query("payment_method_id") String payment_method_id, @Query("issuer.id") String issuer_id);

}
