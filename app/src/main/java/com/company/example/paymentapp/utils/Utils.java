package com.company.example.paymentapp.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

//import static com.google.common.base.Preconditions.checkNotNull;
import com.company.example.paymentapp.R;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class Utils {
    private AlertDialog dialog;
    private Context context;
    private static Utils instance = null;

    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, int frameId) {
        checkNotNull(fragmentManager);
        checkNotNull(fragment);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out);
        transaction.replace(frameId, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    public static Utils getInstance(Context context) {
        if (instance == null) {
            instance = new Utils();
        }
        instance.context = context;
        return instance;
    }


    public void createDialog (
            String title,
            String message,
            String content
    ) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = layoutInflater.inflate(R.layout.dialog, null );
        TextView titleText = (TextView) dialogView.findViewById(R.id.title);
        TextView messageContent = (TextView) dialogView.findViewById(R.id.content);
        TextView messageText = (TextView) dialogView.findViewById(R.id.title_dos);


        titleText.setText(title);
        messageText.setText(content);
        messageContent.setText(message);

        mBuilder.setView(dialogView);
        dialog = mBuilder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }

    public void dismissDialog() {
        if (dialog != null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

}
